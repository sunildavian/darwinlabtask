import {googleImages} from "./googleImages";
import {getGoogleSearchModel} from "./schema";
import {getDB, uploadFilesInMongo, downloadFile} from "./Mongo";

process.on('uncaughtException', function (err) {
    console.log("Uncaught exception comes at>>>>>" + date);
    console.log("err uncaught exception>>>>>>" + err + ">>>>stack>>>>" + err.stack);
});


var configure = (app, context) => {

    app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'access-control-allow-origin, content-type, accept');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    });

    app.options("*", (req, res) => {
        res.writeHead(200, {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "access-control-allow-origin, content-type, accept"
        });
        res.end();
    })


    app.all("/upload", (req, res) => {
        var reqParams = void 0;
        return getRequestParams(req).then(requestParams => {
            reqParams = requestParams;
            uploadFilesOnServer(requestParams, context);
        }).then(_ => {
            writeJSONResponse({result: "Images are uploading once uploaded it will show in search history"}, req, res);
        }).catch(err => {
            writeJSONResponse(err, req, res);
        })
    })

    app.all("/render/:fileKey", (req, res) => {
        var requestParams = void 0;
        return getRequestParams(req).then(params => {
            requestParams = params;
            return getDB(context);
        }).then(dbInstance => {
            return downloadFile(requestParams.fileKey, dbInstance);
        }).then(file => {
            var {data, fileName} = file;
            var options = {ignoreGzip: true, ignoreWrap: true};
            var download = requestParams.download;
            var inline = requestParams.inline || true;
            if (download || inline) {
                var head = {
                    "Cache-Control": "max-age=" + (86400000 * 7) + ",public",
                    "Connection": "keep-alive",
                    "Expires": new Date(Date.now() + (86400000 * 365)).toUTCString()
                };
                head["Content-Disposition"] = (download ? "attachment" : "inline") + "; filename=\"" + fileName + "\"";
                head["Content-Type"] = getContentType(fileName) || file.contentType;
                options["head"] = head;
            }
            writeJSONResponse(data, req, res, options);
        }).catch((error) => {
            writeJSONResponse(error, req, res);
        })
    })

    app.all("/getImageSearchData", (req, res) => {
        var reqParams = void 0;
        return getRequestParams(req).then(requestParams => {
            reqParams = requestParams;
            return getImageSearchData(reqParams, context);
        }).then(response => {
            writeJSONResponse(response, req, res);
        }).catch(err => {
            writeJSONResponse(err, req, res);
        })
    })

}


var writeJSONResponse = (result, req, resp, options = {}) => {
    var jsonResponseType = {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, OPTIONS"
    };
    if (result instanceof Error) {
        var responseToWrite = {
            status: "error"
        };
        var response = {};

        response.message = result.message;
        response.code = result.code;
        responseToWrite.code = 500;
        responseToWrite.response = {
            error: response
        };
        try {
            resp.writeHead(responseToWrite.code, jsonResponseType);
        } catch (err) {
            console.log("err in write error>>>>>>>>>>>>>", err);
        }
        resp.write(JSON.stringify(responseToWrite));
        resp.end();
    } else {
        var customHeader = options.head;
        if (customHeader) {
            jsonResponseType = {
                ...jsonResponseType,
                ...customHeader
            };
        }
        if (!options.ignoreWrap) {
            if (result === undefined) {
                result = null;
            }
            result = JSON.stringify({response: result, status: "ok", code: 200});
        }
        resp.writeHead(200, jsonResponseType);
        resp.write(result);
        resp.end();
    }
};


var getImageSearchData = (params, context) => {
    var {searchKey} = params || {};
    var filter = {searchKey: {$exists: true}};
    if (searchKey) {
        filter["searchKey"] = searchKey;
    }

    var searchModel = getGoogleSearchModel();
    return new Promise((resolve, reject) => {
        searchModel.find(filter, 'searchKey files', function (err, result) {
            if (err) {
                reject(err);
            }
            resolve(result);
        });
    })
};


var getContentType = (fileName) => {
    var extension = fileName
        .split('.')
        .pop();
    var extensionTypes = {
        'css': 'text/css',
        'gif': 'image/gif',
        'jpg': 'image/jpeg',
        'jpeg': 'image/jpeg',
        'js': 'application/javascript',
        'png': 'image/png',
        'mp4': 'video/mp4',
        'mp3': 'audio/mpeg',
        'txt': 'text/plain',
        'pdf': 'application/pdf',
        'xls': 'application/vnd.openxmlformats',
        'xlsx': 'application/vnd.openxmlformats',
        'html': 'text/html',
        'htm': 'text/html'
    };
    if (!extension) {
        return;
    }
    extension = extension.toLowerCase();
    return extensionTypes[extension];
}

var uploadFilesOnServer = (params, context) => {
    var searchKey = params.search && params.search.trim();
    if (!searchKey || searchKey.length === 0) {
        return;
    }
    var files = [];
    var db = void 0;
    return googleImages(searchKey, context).then(bufferFiles => {
        files = bufferFiles;
        return getDB(context);
    }).then(dbInstance => {
        db = dbInstance;
        return uploadFilesInMongo(files, db);
    }).then(filesData => {
        var update = {
            searchKey,
            files: filesData
        }
        return findAndModify({searchKey}, update);
    })

}

var findAndModify = (filter, update) => {
    var searchModel = getGoogleSearchModel();
    return new Promise((resolve, reject) => {
        searchModel.findOneAndUpdate(filter, {$set: update}, {upsert: true}, function (err, doc) {
            if (err) {
                reject(err);
            }
            resolve(doc);
        });
    })
};


var getRequestParams = (req) => {
    return new Promise((resolve, reject) => {
        var allParams = {};
        var params = req.params || {};
        var body = req.body || {};
        var query = req.query || {};
        for (let key in params) {
            if (params.hasOwnProperty(key)) {
                allParams[key] = params[key];
            }
        }
        for (let key in body) {
            if (allParams[key] === void 0) {
                allParams[key] = body[key];
            }
        }
        for (let key in query) {
            if (allParams[key] === void 0) {
                allParams[key] = query[key];
            }
        }
        resolve(allParams);
    })
}

module.exports = {
    configure
};

