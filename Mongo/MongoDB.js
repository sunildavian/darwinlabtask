var MongoClient = require("mongodb").MongoClient;

const getDB = (context, mongoOptions = {}) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(context.MONGO_URL, mongoOptions, (err, db) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(db);
        })
    });
}

module.exports = {
    getDB
}
