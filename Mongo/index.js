import MongoDb from "./MongoDB";
import MongoGrid from "./MongoGrid";

module.exports = {
    ...MongoDb,
    ...MongoGrid
}