import Utilities from "./Utilities"

const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');

const GoogleImages = require('google-images');
var Jimp = require("jimp");

var googleImages = (searchKey, context) => {
    if (!searchKey) {
        return;
    }
    const client = new GoogleImages(context.SEARCH_ENGINE_ID, context.GOOGLE_PROJECT_API);
    var startTime = new Date();
    return client.search(searchKey).then(googleImages => {
        var results = [];
        return Utilities.iterator(googleImages, (index, googleImage) => {
            var imageUrl = googleImage.url;
            return Jimp.read(imageUrl).then(image => {
                return new Promise((resolve, reject) => {
                    if (!image) {
                        resolve();
                    }
                    var modifyImage = image.resize(240, 160).greyscale();
                    modifyImage && modifyImage.getBuffer(image.getMIME(), (err, bufferData) => {
                        if (err) {
                            reject(err);
                        }
                        resolve(bufferData);
                    })
                }).then(bufferData => {
                    if (!bufferData) {
                        return;
                    }
                    return imagemin.buffer(bufferData, {
                        plugins: [
                            imageminJpegtran(),
                            imageminPngquant({quality: '65-80'})
                        ]
                    })
                }).then(compressedData => {
                    if (compressedData) {
                        results.push({
                            filename: imageUrl.substring(imageUrl.lastIndexOf("/") + 1),
                            type: image.getMIME(),
                            data: [new Buffer(compressedData, "base64")]
                        });
                    }
                })
            }).catch(err => {
                console.log("er>>>>>>>>>>", err);
            })
        }).then(_ => {
            return results;
        })

    });

};

module.exports = {
    googleImages
};
